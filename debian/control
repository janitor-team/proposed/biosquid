Source: biosquid
Maintainer: Debian-Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               d-shlibs
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/biosquid
Vcs-Git: https://salsa.debian.org/med-team/biosquid.git
Homepage: http://eddylab.org/software.html
Rules-Requires-Root: no

Package: biosquid
Architecture: any
Depends: ${shlibs:Depends},
         libsquid1 (= ${binary:Version}),
         ${misc:Depends}
Recommends: hmmer
Description: utilities for biological sequence analysis
 SQUID is a library of C code functions for sequence analysis. It also
 includes a number of small utility programs to convert, show statistics,
 manipulate and do other functions on sequence files.
 .
 The original name of the package is "squid", but since there is already
 a squid on the archive (a proxy cache), it was renamed to "biosquid".
 .
 This package contains some tools to demonstrate the features of the
 SQUID library.

Package: libsquid1
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: biosquid
Description: biosquid dynamic library for biological sequence analysis
 SQUID is a library of C code functions for sequence analysis. It also
 includes a number of small utility programs to convert, show statistics,
 manipulate and do other functions on sequence files.
 .
 The original name of the package is "squid", but since there is already
 a squid on the archive (a proxy cache), it was renamed to "biosquid".
 .
 This package contains the dynamic SQUID library.

Package: libsquid-dev
Architecture: any
Section: libdevel
Depends: ${shlibs:Depends},
         libsquid1 (= ${binary:Version}),
         ${misc:Depends}
Breaks: biosquid-dev
Provides: biosquid-dev
Replaces: biosquid-dev
Description: biosquid headers and static library for biological sequence analysis
 SQUID is a library of C code functions for sequence analysis. It also
 includes a number of small utility programs to convert, show statistics,
 manipulate and do other functions on sequence files.
 .
 The original name of the package is "squid", but since there is already
 a squid on the archive (a proxy cache), it was renamed to "biosquid".
 .
 This package contains the header files and the static SQUID library.
