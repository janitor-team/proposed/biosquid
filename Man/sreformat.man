.TH "sreformat" 1 "@RELEASEDATE@" "@PACKAGE@ @RELEASE@" "@PACKAGE@ Manual"

.SH NAME
.TP 
sreformat - convert sequence file to different format

.SH SYNOPSIS
.B sreformat
.I [options]
.I format
.I seqfile

.SH DESCRIPTION

.B sreformat
reads the sequence file
.I seqfile
in any supported format, reformats it
into a new format specified by 
.I format,
then prints the reformatted text.

.PP
Supported input formats include (but are not limited to) the unaligned
formats FASTA, Genbank, EMBL, SWISS-PROT, PIR, and GCG, and the
aligned formats Stockholm, Clustal, GCG MSF, and Phylip.

.PP
Available unaligned output file format codes 
include
.I fasta
(FASTA format);
.I embl
(EMBL/SWISSPROT format);
.I genbank
(Genbank format);
.I gcg
(GCG single sequence format);
.I gcgdata
(GCG flatfile database format);
.I pir
(PIR/CODATA flatfile format);
.I raw
(raw sequence, no other information).

.pp
The available aligned output file format
codes include
.I stockholm
(PFAM/Stockholm format);
.I msf
(GCG MSF format); 
.I a2m
(an aligned FASTA format);
.I PHYLIP
(Felsenstein's PHYLIP format); and
.I clustal
(Clustal V/W/X format); and
.I selex
(the old SELEX/HMMER/Pfam annotated alignment format);

.pp
All thee codes are interpreted case-insensitively
(e.g. MSF, Msf, or msf all work).

.PP 
Unaligned format files cannot be reformatted to
aligned formats.
However, aligned formats can be reformatted
to unaligned formats -- gap characters are 
simply stripped out.

.PP
This program was originally named
.B reformat,
but that name clashes with a GCG program of the same name.

.SH OPTIONS

.TP
.B -d 
DNA; convert U's to T's, to make sure a nucleic acid
sequence is shown as DNA not RNA. See
.B -r.

.TP
.B -h
Print brief help; includes version number and summary of
all options, including expert options.

.TP
.B -l
Lowercase; convert all sequence residues to lower case.
See
.B -u.

.TP
.B -n
For DNA/RNA sequences, converts any character that's not unambiguous
RNA/DNA (e.g. ACGTU/acgtu) to an N. Used to convert IUPAC ambiguity
codes to N's, for software that can't handle all IUPAC codes (some
public RNA folding codes, for example). If the file is an alignment,
gap characters are also left unchanged. If sequences are not
nucleic acid sequences, this option will corrupt the data in
a predictable fashion.

.TP
.B -r 
RNA; convert T's to U's, to make sure a nucleic acid
sequence is shown as RNA not DNA. See
.B -d.

.TP
.B -u
Uppercase; convert all sequence residues to upper case.
See
.B -l.

.TP
.B -x
For DNA sequences, convert non-IUPAC characters (such as X's) to N's.
This is for compatibility with benighted people who insist on using X
instead of the IUPAC ambiguity character N. (X is for ambiguity
in an amino acid residue). 
.IP
Warning: like the
.B -n
option, the code doesn't check that you are actually giving it DNA. It
simply literally just converts non-IUPAC DNA symbols to N. So if you
accidentally give it protein sequence, it will happily convert most
every amino acid residue to an N.

.SH EXPERT OPTIONS

.TP
.BI --gapsym " <c>"
Convert all gap characters to 
.I <c>.
Used to prepare alignment files for programs with strict
requirements for gap symbols. Only makes sense if
the input 
.I seqfile
is an alignment.

.TP
.BI --informat " <s>"
Specify that the sequence file is in format 
.I <s>,
rather than allowing the program to autodetect
the file format. Common examples include Genbank, EMBL, GCG, 
PIR, Stockholm, Clustal, MSF, or PHYLIP; 
see the printed documentation for a complete list
of accepted format names.

.TP
.B --mingap
If 
.I seqfile
is an alignment, remove any columns that contain 100% gap
characters, minimizing the overall length of the alignment.
(Often useful if you've extracted a subset of aligned
sequences from a larger alignment.)

.TP
.B --nogap
Remove any aligned columns that contain any gap symbols
at all. Useful as a prelude to phylogenetic analyses, where
you only want to analyze columns containing 100% residues,
so you want to strip out any columns with gaps in them.
Only makes sense if the file is an alignment file.


.TP
.B --pfam
For SELEX alignment output format only, put the entire
alignment in one block (don't wrap into multiple blocks).
This is close to the format used internally by Pfam
in Stockholm and Cambridge.

.TP
.B --sam
Try to convert gap characters to UC Santa Cruz SAM style, where a .
means a gap in an insert column, and a - means a
deletion in a consensus/match column. This only
works for converting aligned file formats, and only
if the alignment already adheres to the SAM convention
of upper case for residues in consensus/match columns,
and lower case for residues in insert columns. This is
true, for instance, of all alignments produced by old
versions of HMMER. (HMMER2 produces alignments
that adhere to SAM's conventions even in gap character choice.)
This option was added to allow Pfam alignments to be
reformatted into something more suitable for profile HMM
construction using the UCSC SAM software.

.TP
.BI --samfrac " <x>"
Try to convert the alignment gap characters and
residue cases to UC Santa Cruz SAM style, where a .
means a gap in an insert column and a - means a
deletion in a consensus/match column, and 
upper case means match/consensus residues and
lower case means inserted resiudes. This will only
work for converting aligned file formats, but unlike the
.B --sam 
option, it will work regardless of whether the file adheres
to the upper/lower case residue convention. Instead, any 
column containing more than a fraction 
.I <x> 
of gap characters is interpreted as an insert column,
and all other columns are interpreted as match columns.
This option was added to allow Pfam alignments to be
reformatted into something more suitable for profile HMM
construction using the UCSC SAM software.

.TP
.B --wussify
Convert RNA secondary structure annotation strings (both consensus
and individual) from old "KHS" format, ><, to the new WUSS notation,
<>. If the notation is already in WUSS format, this option will screw it
up, without warning. Only SELEX and Stockholm format files have
secondary structure markup at present.

.TP
.B --dewuss
Convert RNA secondary structure annotation strings from the new
WUSS notation, <>, back to the old KHS format, ><. If the annotation
is already in KHS, this option will corrupt it, without warning.
Only SELEX and Stockholm format files have secondary structure
markup.

.SH SEE ALSO

.PP
@SEEALSO@

.SH AUTHOR

@PACKAGE@ and its documentation are @COPYRIGHT@
@LICENSE@
See COPYING in the source code distribution for more details, or contact me.

.nf
Sean Eddy
HHMI/Department of Genetics
Washington University School of Medicine
4444 Forest Park Blvd., Box 8510
St Louis, MO 63108 USA
Phone: 1-314-362-7666
FAX  : 1-314-362-2157
Email: eddy@genetics.wustl.edu
.fi


